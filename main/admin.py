from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from main.models import Client, Mailing, Message


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
	list_display = ['id', 'phone', 'code', 'tag']


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
	list_display = ['id', 'start_dt', 'stop_dt', 'text', 'code_filter', 'tag_filter', 'status']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
	list_display = ['id', 'mailing_link', 'client', 'created_at', 'status']

	def mailing_link(self, obj: Message):
		url = reverse("admin:main_mailing_change", args=[obj.mailing.id])
		link = '<a href="%s">%s</a>' % (url, obj.mailing)
		return mark_safe(link)

