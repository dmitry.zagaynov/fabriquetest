from django.db import models


class Mailing(models.Model):
	class Status(models.TextChoices):
		PENDING = ('p', 'pending')
		FINISHED = ('f', 'finished')

	start_dt = models.DateTimeField()
	stop_dt = models.DateTimeField()
	text = models.TextField()
	code_filter = models.CharField(max_length=10)
	tag_filter = models.CharField(max_length=50)
	status = models.CharField(
		max_length=1, choices=Status.choices, default=Status.PENDING)


class Client(models.Model):
	phone = models.CharField(max_length=11)
	code = models.CharField(max_length=10)
	tag = models.CharField(max_length=50)
	timezone = models.SmallIntegerField()


class Message(models.Model):
	class Status(models.TextChoices):
		PENDING = ('p', 'pending')
		SUCCESS = ('s', 'success')
		FAILED = ('f', 'failed')
	mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
	client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')
	created_at = models.DateTimeField(auto_now=True)
	status = models.CharField(max_length=10, choices=Status.choices, default=Status.PENDING)
