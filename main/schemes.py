import uuid

from django.conf import settings
from drf_spectacular.utils import extend_schema, OpenApiExample, OpenApiParameter

from main.serializers import ClientSerializer, ErrorSerializer, MailingSerializer, DetailStatsSerializer, \
	GeneralStatsSerializer

CLIENT_UUID = OpenApiParameter(
	name='id',
	description='Уникальный UUID клиента',
	type=uuid.UUID,
	location='path',
	required=True
)

CLIENT_NOT_FOUND = OpenApiExample(
	name='Клиент не найден',
	description='Клиент с данным UUID не существует',
	value={'detail': 'Страница не найдена'},
	response_only=True,
	status_codes=[404]
)

WRONG_PHONE_FORMAT = OpenApiExample(
	name='Неверный формат номера телефона',
	description='Ошибка в запросе',
	value={
		'phone': [
			'Wrong format. Use this format instead: '
			'7XXXXXXXXXX (X - digit from 0 to 9).'
		]
	},
	response_only=True,
	status_codes=[400]
)

WRONG_TIMEZONE_FORMAT = OpenApiExample(
	name='Неверный формат часового пояса',
	description='Ошибка в запросе',
	value={
		'timezone': [
			'Ensure this value in range -12..12'
		]
	},
	response_only=True,
	status_codes=[400]
)

CLIENT_SCHEMA = {
	'retrieve': extend_schema(
		summary='Просмотр данных клиента',
		parameters=[CLIENT_UUID],
		responses={
			200: ClientSerializer(),
			404: ErrorSerializer()
		},
		examples=[CLIENT_NOT_FOUND]
	),
	'create': extend_schema(
		summary='Добавление нового клиента в справочник со всеми его атрибутами',
		responses={
			201: ClientSerializer(),
			400: ErrorSerializer()
		},
		examples=[
			OpenApiExample(
				name='Пример запроса',
				description="""
					phone: телефон в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
					code: код мобильного оператора
					tag: тег (произвольная метка)
					timezone: часовой пояс (сдвиг относительно UTC от -12 до 1)
				""",
				request_only=True,
			),
			OpenApiExample(
				name='Клиент успешно создан',
				response_only=True,
				status_codes=[201]
			),
			WRONG_PHONE_FORMAT,
			WRONG_TIMEZONE_FORMAT
		]
	),
	'partial_update': extend_schema(
		summary='Обновление данных атрибутов клиента',
		parameters=[CLIENT_UUID],
		responses={
			200: ClientSerializer(),
			400: ErrorSerializer(),
			404: ErrorSerializer()
		},
		examples=[
			OpenApiExample(
				name='Данные клиента успешно изменены',
				response_only=True,
				status_codes=[200]
			),
			WRONG_PHONE_FORMAT,
			WRONG_TIMEZONE_FORMAT,
			CLIENT_NOT_FOUND
		]
	),
	'destroy': extend_schema(
		summary='Удаление клиента из справочника',
		parameters=[CLIENT_UUID],
		responses={
			204: None,
			404: ErrorSerializer()
		},
		examples=[CLIENT_NOT_FOUND]
	)
}

if settings.DEBUG:
	CLIENT_SCHEMA.update({'list': extend_schema(summary='Просмотр списка клиентов')})

MAILING_ID = OpenApiParameter(
	name='id',
	description='Уникальный ID рассылки',
	type=int,
	location='path',
	required=True
)

MAILING_NOT_FOUND = OpenApiExample(
	name='Рассылка не существует',
	description='Рассылка с данным ID не существует',
	value={'detail': 'Страница не найдена'},
	response_only=True,
	status_codes=[404]
)

MAILING_SCHEME = {
	'retrieve': extend_schema(
		summary='Просмотр данных рассылки',
		parameters=[MAILING_ID],
		responses={
			200: MailingSerializer(),
			404: ErrorSerializer()
		},
		examples=[MAILING_NOT_FOUND]
	),
	'list': extend_schema(summary='Просмотр списка рассылок'),
	'create': extend_schema(
		summary='Создание новой рассылки',
		responses={
			201: MailingSerializer(),
			400: ErrorSerializer()
		},
		examples=[
			OpenApiExample(
				name='Пример запроса',
				description="""
					start_dt: дата и время старта рассылки (строка в формате YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]).
					stop_dt: дата и время окончания рассылки
					text: текст сообщения для доставки клиенту
					code_filter: фильтр клиентов по коду оператора 
					tag_filter: фильтр клиентов по тэгу
				""",
				request_only=True,
			),
			OpenApiExample(
				name='Рассылка успешно создана',
				response_only=True,
				status_codes=[201]
			),
		]
	),
	'partial_update': extend_schema(
		summary='Обновление атрибутов рассылки',
		parameters=[MAILING_ID],
		responses={
			200: MailingSerializer(),
			400: ErrorSerializer(),
			404: ErrorSerializer()
		},
		examples=[
			OpenApiExample(
				name='Данные рассылки успешно изменены',
				response_only=True,
				status_codes=[200]
			),
			MAILING_NOT_FOUND
		]
	),
	'destroy': extend_schema(
		summary='Удаление рассылки',
		parameters=[MAILING_ID],
		responses={
			204: None,
			404: ErrorSerializer()
		},
		examples=[MAILING_NOT_FOUND]
	)
}

DETAIL_STATS_SCHEME = {
	'operation_id': 'detail_stats_retrieve',
	'responses': DetailStatsSerializer,
	'summary': 'Детальная статистика по рассылке',
	'parameters': [MAILING_ID],
	'examples': [
		OpenApiExample(
			name='Пример ответа',
			description="""
					success_messages: Кол-во успешно отправленных сообщений
					failed_messages: Кол-во неотправенных сообщений
					success_rate: Процент успешной отправки
					total_clients: Общее кол-во клиентов в рассылке
				""",
			response_only=True,
			status_codes=[200]
		)
	]
}

GENERAL_STATS_SCHEME = {
	'operation_id': 'general_stats_retrieve',
	'responses': GeneralStatsSerializer,
	'summary': 'Статистика по всем рассылкам',
	'examples': [
		OpenApiExample(
			name='Пример ответа',
			description="""
					success_messages: Кол-во успешно отправленных сообщений
					failed_messages: Кол-во неотправенных сообщений
					total_clients: Общее кол-во клиентов
					total_mailings: Общее кол-во завершённых рассылок
				""",
			response_only=True,
			status_codes=[200]
		)
	]
}
