import re

from django.utils.translation import gettext as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from main.models import Mailing, Client


class MailingSerializer(serializers.ModelSerializer):
	status = serializers.CharField(source='get_status_display', read_only=True)

	class Meta:
		model = Mailing
		fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Client
		fields = '__all__'

	def validate_phone(self, value):
		pattern = re.compile(r'^7\d{10}')
		if not pattern.match(value):
			raise ValidationError(
				'Wrong format. Use this format instead: 7XXXXXXXXXX (X - digit from 0 to 9).')
		return value

	def validate_timezone(self, value):
		if value not in range(-12, 12):
			raise ValidationError('Ensure this value in range -12..12')
		return value


class ErrorSerializer(serializers.Serializer):
	field = serializers.ListSerializer(child=serializers.CharField())


class StatsSerializer(serializers.Serializer):
	success_messages = serializers.IntegerField()
	failed_messages = serializers.IntegerField()
	success_rate = serializers.FloatField()


class DetailStatsSerializer(StatsSerializer):
	total_clients = serializers.IntegerField()


class GeneralStatsSerializer(StatsSerializer):
	total_mailings = serializers.IntegerField()

