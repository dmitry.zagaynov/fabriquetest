from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now
from django_celery_beat.models import PeriodicTask, ClockedSchedule

from main import tasks
from main.models import Mailing, Client


@receiver(post_save, sender=Mailing)
def start_mailing(sender, instance, created, **kwargs):
	if created:
		if instance.start_dt < now() < instance.stop_dt:
			tasks.start_mailing.delay(instance.id)
		elif instance.start_dt > now() < instance.stop_dt:
			PeriodicTask.objects.create(
				name=f'{instance.id}_MAILING',
				task='main.tasks.start_mailing',
				args=[instance.id],
				clocked=ClockedSchedule.objects.create(clocked_time=instance.start_dt),
				one_off=True
			)
