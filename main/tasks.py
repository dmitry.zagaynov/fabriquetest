import json
import asyncio
import aiohttp
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.utils.timezone import now

from main.models import Mailing, Client, Message


@shared_task
def start_mailing(mailing_id):
	logger = get_task_logger(__name__)
	results = []
	payloads = []

	mailing = Mailing.objects.get(pk=mailing_id)
	clients = Client.objects.filter(
		tag=mailing.tag_filter,
		code=mailing.code_filter,

	)
	for client in clients:
		message = Message.objects.create(
			mailing_id=mailing_id,
			client_id=client.id
		)
		body = {
			'id': message.pk,
			'phone': client.phone,
			'text': mailing.text
		}
		payloads.append((f'{settings.MAIL_SERVICE_URL}/{message.id}', body))

	async def gather_with_concurrency(n):
		semaphore = asyncio.Semaphore(n)
		headers = {'Authorization': f'Bearer {settings.MAIL_SERVICE_TOKEN}'}
		session = aiohttp.ClientSession(connector=conn, headers=headers)

		async def post(payload):
			async with semaphore:
				msg = await Message.objects.aget(pk=payload[1]['id'])
				if mailing.stop_dt > now():
					async with session.post(payload[0], json=payload[1]) as response:
						response = json.loads(await response.read())
						results.append(response)
						if 'message' not in response or response['message'] != 'OK':
							msg.status = Message.Status.FAILED
							logger.info(
								f'Message id={msg.id} (Mailing id={mailing.id}, '
								f'created_at={msg.created_at}) failed. Server error: ({response}).'
							)
						else:
							msg.status = Message.Status.SUCCESS
							logger.info(
								f'Message id={msg.id} (Mailing id={mailing.id}, '
								f'created_at={msg.created_at}) sent successfully.'
							)
				else:
					msg.status = Message.Status.FAILED
					logger.info(
						f'Message id={msg.id} (Mailing id={mailing.id}, '
						f'created_at={msg.created_at}) ) failed. Mailing stopped.'
					)
				await msg.asave()

		await asyncio.gather(*(post(payload) for payload in payloads))
		await session.close()

	conn = aiohttp.TCPConnector(limit=100)
	loop = asyncio.get_event_loop()
	parallel_requests = int(settings.PARALLEL_REQUESTS)
	loop.run_until_complete(gather_with_concurrency(parallel_requests))
	conn.close()
	mailing.status = Mailing.Status.FINISHED
	mailing.save()

	logger.info(f'Sent messages: {len(results)}')
