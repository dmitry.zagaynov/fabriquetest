from django.urls import path, include
from rest_framework.routers import SimpleRouter

from main.views import MailingViewSet, ClientViewSet

r = SimpleRouter()
r.register('mailings', MailingViewSet)
r.register('clients', ClientViewSet)

urlpatterns = [
	path('', include(r.urls))
]
