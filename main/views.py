from django.http import JsonResponse
from drf_spectacular.utils import extend_schema_view, extend_schema
from rest_framework import viewsets
from rest_framework.decorators import action

from main.schemes import CLIENT_SCHEMA, MAILING_SCHEME, DETAIL_STATS_SCHEME, GENERAL_STATS_SCHEME
from main.serializers import MailingSerializer, ClientSerializer
from main.models import Mailing, Client, Message


@extend_schema_view(**MAILING_SCHEME)
class MailingViewSet(viewsets.ModelViewSet):
	queryset = Mailing.objects.all()
	serializer_class = MailingSerializer
	http_method_names = ['get', 'post', 'patch', 'delete']

	@extend_schema(**DETAIL_STATS_SCHEME)
	@action(methods=['GET'], detail=True, url_path='stats', url_name='detail_stats')
	def detail_stats(self, request, pk):
		msg = Message.objects.select_related('mailing').filter(mailing_id=pk)
		success_messages = msg.filter(status=Message.Status.SUCCESS).count()
		failed_messages = msg.filter(status=Message.Status.FAILED).count()
		clients_count = Client.objects.prefetch_related('messages') \
			.filter(messages__mailing_id=pk).count()
		if failed_messages == 0:
			rate = 1
		elif success_messages == 0:
			rate = 0
		else:
			rate = success_messages / clients_count
		stats = {
			'total_clients': clients_count,
			'success_messages': success_messages,
			'failed_messages': failed_messages,
			'success_rate': round(rate * 100, 2)
		}
		return JsonResponse(stats)

	@extend_schema(**GENERAL_STATS_SCHEME)
	@action(methods=['GET'], detail=False, url_path='stats', url_name='general_stats')
	def general_stats(self, request):
		mailings = Mailing.objects.values_list('id', flat=True)
		ms = Message.objects.select_related('mailing').filter(mailing_id__in=mailings)
		stats = {
			'total_mailings': mailings.count(),
			'total_clients': Client.objects.prefetch_related('messages')
			.filter(messages__in=ms).distinct().count(),
			'success_messages': ms.filter(status=Message.Status.SUCCESS).count(),
			'failed_messages': ms.filter(status=Message.Status.FAILED).count()
		}
		return JsonResponse(stats)


@extend_schema_view(**CLIENT_SCHEMA)
class ClientViewSet(viewsets.ModelViewSet):
	queryset = Client.objects.all()
	serializer_class = ClientSerializer
	http_method_names = ['get', 'post', 'patch', 'delete']
